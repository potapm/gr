import random
import gradio as gr
import pandas as pd
import seaborn as sbn
# pip install openpyxl
from PyQt5.QtWidgets import *
import openpyxl
import datetime
from pathlib import Path
from configs.config import MainConfig
from confz import BaseConfig, FileSource
from PIL import Image
import torch
import numpy as np
from tqdm import tqdm
from utils.utils import load_detector, load_classificator, open_mapping, extract_crops
from itertools import repeat
import os
from zipfile import ZipFile

absolute_path = "Wrong path"


class Photo:
    def __init__(self, path, dateStart, dateEnd, _class, count, idTrap):
        self.filePath = path
        self.dateStart = dateStart
        self.dateEnd = dateEnd
        self.Class = _class
        self.Count = count
        self.idTrap = idTrap
        self.validClass = True


def getGraph(_all):
    dct = {}
    for x in _all:
        date = datetime.datetime(x.dateStart.year, x.dateStart.month, x.dateStart.day)
        if dct.get(date, -1) == -1:
            dct[date] = {}
        type = x.Class
        if dct[date].get(type, -1) == -1:
            dct[date][type] = 0
        dct[date][type] += x.Count
    arr = []
    for d in dct.keys():
        for t in dct[d].keys():
            arr.append([d, t, dct[d][t]])
    timeDf = pd.DataFrame(data=arr, columns=['date', 'type', 'count'])
    plot = sbn.scatterplot(data=timeDf, x="date", y="count", hue="type", palette="deep")
    fig = plot.get_figure()
    fig.autofmt_xdate()
    fig.savefig('graph.png')
    return fig


def getCsv(_all):
    arr = [[x.idTrap, x.Class, x.dateStart, x.dateEnd, x.Count] for x in _all]
    df = pd.DataFrame(data=arr, columns='ловушка,класс,дата старта,дата окончания,количество'.split(','))
    df.to_csv('file.csv', index=False)
    return df


def getExcel():
    wb = openpyxl.Workbook()
    ws = wb.active
    img = openpyxl.drawing.image.Image('graph.png')
    img.anchor(ws.cell('A1'))
    ws.add_image(img)
    wb.save('output.xlsx')


def check(path):
    try:
        im = Image.open(path)
    except IOError:
        return 0
    return 1


def get_all_file_paths(directory):
    file_paths = []

    for root, directories, files in os.walk(directory):
        for filename in files:
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)

    return file_paths


def go_model():
    # Load main config
    main_config = MainConfig(config_sources=FileSource(file=os.path.join("configs", "config.yml")))
    device = main_config.device
    # Load imgs from source dir
    # pathes_to_imgs = [i for i in Path(main_config.src_dir).glob("*")
    #                  if i.suffix.lower() in [".jpeg", ".jpg", ".png"]]
    pathes_to_imgs = [os.path.join(dp, f) for dp, dn, filenames in os.walk(Path(main_config.src_dir)) for f in filenames
                      if os.path.splitext(f)[1].lower() in [".jpeg", ".jpg", ".png"]]
    print(pathes_to_imgs)

    # Load mapping for classification task
    mapping = open_mapping(path_mapping=main_config.mapping)
    # Separate main config
    detector_config = main_config.detector
    classificator_config = main_config.classificator
    # Load models
    detector = load_detector(detector_config).to(device)
    classificator = load_classificator(classificator_config).to(device)
    list_predictions = []
    # Inference
    if len(pathes_to_imgs):
        num_packages_det = np.ceil(len(pathes_to_imgs) / detector_config.batch_size).astype(np.int32)
        with torch.no_grad():
            for i in tqdm(range(num_packages_det), colour="green"):
                # Inference detector
                batch_images_det = pathes_to_imgs[detector_config.batch_size * i:
                                                  detector_config.batch_size * (1 + i)]
                results_det = detector(batch_images_det,
                                       iou=detector_config.iou,
                                       conf=detector_config.conf,
                                       imgsz=detector_config.imgsz,
                                       verbose=False,
                                       device=device)
                if len(results_det) > 0:
                    # Extract crop by bboxes
                    dict_crops = extract_crops(results_det, config=classificator_config)
                    # Inference classificator
                    for img_name, batch_images_cls in dict_crops.items():
                        # if len(batch_images_cls) > classificator_config.batch_size:
                        num_packages_cls = np.ceil(len(batch_images_cls) / classificator_config.batch_size).astype(
                            np.int32)
                        for j in range(num_packages_cls):
                            batch_images_cls = batch_images_cls[classificator_config.batch_size * j:
                                                                classificator_config.batch_size * (1 + j)]
                            logits = classificator(batch_images_cls.to(device))
                            probabilities = torch.nn.functional.softmax(logits, dim=1)
                            top_p, top_class_idx = probabilities.topk(1, dim=1)
                            # Locate torch Tensors to cpu and convert to numpy
                            top_p = top_p.cpu().numpy().ravel()
                            top_class_idx = top_class_idx.cpu().numpy().ravel()
                            class_names = [mapping[top_class_idx[idx]] for idx, _ in enumerate(batch_images_cls)]
                            list_predictions.extend([[name, cls, prob] for name, cls, prob in
                                                     zip(repeat(img_name, len(class_names)), class_names, top_p)])

    # Create Dataframe with predictions
    table = pd.DataFrame(list_predictions, columns=["image_name", "class_name", "confidence"])
    # table.to_csv("table.csv", index=False) # Раскомментируйте, если хотите увидеть результаты предсказания
    # нейронной сети по каждому найденному объекту
    agg_functions = {
        'class_name': ['count'],
        "confidence": ["mean"]
    }
    groupped = table.groupby(['image_name', "class_name"]).agg(agg_functions)
    img_names = groupped.index.get_level_values("image_name").unique()
    final_res = []
    for img_name in img_names:
        groupped_per_img = groupped.query(f"image_name == '{img_name}'")
        max_num_objects = groupped_per_img["class_name", "count"].max()
        # max_confidence = groupped_per_img["class_name", "confidence"].max()
        statistic_by_max_objects = groupped_per_img[groupped_per_img["class_name", "count"] == max_num_objects]
        if len(statistic_by_max_objects) > 1:
            # statistic_by_max_mean_conf = statistic_by_max_objects.reset_index().max().values
            statistic_by_max_mean_conf = statistic_by_max_objects.loc[
                [statistic_by_max_objects["confidence", "mean"].idxmax()]]
            final_res.extend(statistic_by_max_mean_conf.reset_index().values)
        else:
            final_res.extend(statistic_by_max_objects.reset_index().values)
    groupped.to_csv("table_agg.csv", index=True)  # Раскомментируйте, если хотите увидеть результаты аггрегации
    final_table = pd.DataFrame(final_res, columns=["image_name", "class_name", "count", "confidence"])
    final_table.to_csv("final_table.csv", index=False)
    final_table['date_registration'] = ''
    for i in final_table.index:
        final_table.loc[i, ('date_registration')] = Image.open(Path(pathes_to_imgs[i])).getexif().get(
            306)

    final_table['date_registration'] = pd.to_datetime(final_table['date_registration'],
                                                      format='%Y:%m:%d %H:%M:%S')
    final_table['name_folder'] = ''
    try:
        final_table['name_folder'] = final_table['image_name'].str.split('/', expand=True)[8]
    except:
        pass
    final_table = final_table[['name_folder', 'date_registration', 'image_name', 'class_name', 'count', 'confidence']]
    final_table['image_name2'] = ''
    try:
        final_table['image_name2'] = final_table['image_name'].str.split('/', expand=True)[9].str.lower()
    except:
        pass
    ################# Окном в 10 минут усредняем класс в группе
    for i in final_table['name_folder'].unique():
        for j in final_table[final_table['name_folder'] == i].index:
            dr = final_table.loc[j]['date_registration']
            win = final_table[(final_table['date_registration'] >= dr) & (
                    final_table['date_registration'] <= dr + pd.Timedelta(minutes=10))]
            if len(win) > 5:
                if len(win['class_name'].unique()) > 1:
                    win_t = win
                    final_table['class_name'].loc[win.index] = pd.Series.mode(win['class_name'])[0]
        print(i)
    ################

    final_table = final_table.sort_values(by=['name_folder', 'date_registration'], ascending=True).reset_index(
        drop=True)
    final_table['isstart'] = final_table['date_registration'].diff() > pd.Timedelta(minutes=30)
    final_table['isstart'][(final_table['name_folder'] != final_table['name_folder'].shift(1))] = 1
    final_table['registrations_id'] = final_table['isstart'].cumsum()

    rez_registrations_id = final_table.sort_values(by=['date_registration', 'name_folder'], ascending=True).reset_index(
        drop=True)

    gb = final_table.groupby(['registrations_id'])
    sub = gb.agg(
        name_folder=('name_folder', np.max),
        clas=('class_name', lambda x: pd.Series.mode(x)[0]),
        date_registration_start=('date_registration', np.min),
        date_registration_end=('date_registration', np.max),
        count=('count', np.max)).reset_index()

    sub['count'][sub['count'] > 5] = 5
    for i, x in enumerate(pathes_to_imgs):
        sub['name_folder'][i] = str(Path(x).parts[-2])
    sub.to_csv("./submit.csv", index=False)
    return True


def zipunarchive(path):
    with ZipFile(path, 'r') as f:
        f.extractall(path='./data')


def func(file):
    print(file)
    zipunarchive(file)
    go_model()
    _all = []
    # file = [x for x in file if check(x)]
    DF = pd.read_csv('submit.csv')
    for i in range(len(DF)):
        x = ''
        _timeStart = DF['date_registration_start'][i]
        _timeEnd = DF['date_registration_end'][i]
        _class = DF['clas'][i]
        _count = DF['count'][i]
        _id = DF['name_folder'][i]
        _all.append(
            Photo(path=x, dateStart=_timeStart, dateEnd=_timeEnd, _class=_class, count=_count, idTrap=_id))
    # graph
    # fig = getGraph(_all)
    # csv
    table = getCsv(_all)
    return (table,
            'file.csv',
            # fig
            )


if __name__ == "__main__":
    inputs = [
        gr.File(),
    ]
    outputs = [gr.DataFrame(label="Таблица"),
               gr.File(label="Результат"),
               # gr.Plot(label="График"),
               ]
    demo = gr.Interface(fn=func, inputs=inputs, outputs=outputs, clear_btn="Очистить", submit_btn="Загрузить",
                        live=False)
    demo.launch(share=False, inbrowser=True)
